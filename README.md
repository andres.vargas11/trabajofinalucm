# Trabajo Final 

Este repositorio de __GITLAB__ se creo con el objetivo de compartir de forma academica el trabajo final.


__Materia:      Desarrollo Para El PenTesting__    
__Profesor:     Cesar Muñoz Araya__  
__Estudiante:   Andrés Mauricio Vargas Gil__     
__Especialización en Ciberseguridad__  
__Universidad Católica de Manizalez__ 
    
__Repositorio GIT https://gitlab.com/andres.vargas11/trabajofinalucm.git__  


__Definición__

<p style="text-align: justify;"> 
Este ejericio academico emula el papel de un atacante bien sea un empleado o contratista, un proveedor con acceso a la red que intenta obtener acceso a uno o varios sistemas de información utilizando técnicas de penetración para extraer datos confidenciales, la tecnica a utilizar es un ataque de hombre en el medio (MitM) desarrollada para recopilar información de usuarios y contraseñas a cualquier aplicación web que utilice el protocolo HTTP. </p>  

__MitM__
<p style="text-align: justify;"> 
Man-in-the-Middle (MitM), que en español significa “hombre en el medio”, es un tipo de ataque destinado a interceptar, sin autorización, la comunicación entre dos dispositivos (hosts) conectados a una red. Este ataque le permite a un agente malintencionado manipular el tráfico interceptado de diferentes formas, ya sea para escuchar la comunicación y obtener información sensible, como credenciales de acceso, información financiera, etc., o para suplantar la identidad de alguna de las partes. Para que un ataque MitM funcione correctamente, el delincuente debe asegurarse que será el único punto de comunicación entre los dos dispositivos, es decir, el delincuente debe estar presente en la misma red que los hosts apuntados en el ataque para cambiar la tabla de enrutamiento para cada uno de ellos. </p>

__Requisitos y Escenario__  

Conexion a Internet   
Portatil Windows   
Hypervisor Vmware WorkStation    
Maquina Virtual Kali Linux __(ATACANTE)__  
Python v.3.10.4   
  - JupyterLab v3.4.3
  - Libreria Scapy v.2.4.5   
  - Libreria Scapy_HTTP  v.1.8.2  
Maquina Virtual Windows 7 __(VICTIMA)__  



Para la ejecucion de este ejercicio academico es necesario que el __vm_ATACANTE__ y la __vm_VICTIMA__ esten en el mismo segmento de RED (172.20.10.0) como se evidencia en la siguiente imangen:

![1](EsquemaAtaque.png)

En la grafica anterior observamos que el ataque inicia con la ejecucion de 2 scripts codificados en python que comprometen la victima, obteniendo información confidencial cuando realiza la autenticacion en el sitio web

__arp_spoofing.py__      ----> Modificas las tablas ARP de la Victima y el Gateway  
__password_sniffer.py__  ----> Captura Tráfico HTTP y Guarda Credenciales (Usuario y Contraseña)"
