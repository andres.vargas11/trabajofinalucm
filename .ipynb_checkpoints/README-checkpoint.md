# TrabajoFinalUCM 

Este Repositorio de GITLAB se creo con el objetivo de compartir el trabajo final de la materia "Desarrollo para el PenTesting" / Especialización en Ciberseguridad / Universidad Católica de Manizalez

Profesor == Cesar Maraya

Alumno   == Andres Mauricio Vargas Gil 

Link del Repositorio == https://gitlab.com/andres.vargas11/trabajofinalucm.git

Definicion

Un test de penetración emula el papel de un atacante bien sea de un empleado de la empresa o un proveedor con acceso a la red que intenta obtener acceso a uno o varios sistemas de información utilizando técnicas de penetración para extraer información adicional como nombres de las bases de datos, datos confidenciales de la empresa, correos electrónicos e información de los empleados. Este trabajo emula un ataque de hombre en el medio (MitM) desarrollado para recopilar información confidencial de usuarios y contraseñas a cualquier sitio web que utilice el protocolo HTTP.  

MitM

Man-in-the-Middle (MitM), que en español significa “hombre en el medio”, es un tipo de ataque destinado a interceptar, sin autorización, la comunicación entre dos dispositivos (hosts) conectados a una red. Este ataque le permite a un agente malintencionado manipular el tráfico interceptado de diferentes formas, ya sea para escuchar la comunicación y obtener información sensible, como credenciales de acceso, información financiera, etc., o para suplantar la identidad de alguna de las partes. Para que un ataque MitM funcione correctamente, el delincuente debe asegurarse que será el único punto de comunicación entre los dos dispositivos, es decir, el delincuente debe estar presente en la misma red que los hosts apuntados en el ataque para cambiar la tabla de enrutamiento para cada uno de ellos.

Escenario y Requisitos

Conexion a Internet 
Host Windows 11 / Vmware WorkStation /
Maquina Virtual Kali Linux (ATACANTE) / 
Maquina Virtual Windows 7 (VICTIMA)
Scripts Desarrollados en Python

Es necesario que el ATACANTE y la VICTIMA esten en el mismo segmento de RED

![1](EsquemaAtaque.png)

En la grafica anterior observamos que el ataque inicia con la ejecucion de 2 programas codificados en python que compromenten la victima obteniendo información confidencial de usuarios y contraseñas.
